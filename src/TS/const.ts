export const canvas = document.querySelector('canvas')
export const c = canvas.getContext('2d')
export const gravity = 0.05

export const keys = {
    d: {
        pressed: false
    },
    q: {
        pressed: false
    }, 
    s: {
        pressed: false
    },
    z: {
        pressed :false},
    

}

export const scaleBG = 3

export const scaledCanvas = {
    width: canvas.width / scaleBG,
    height: canvas.height / scaleBG
}

export const collision2D: number[][] = []

export const camera = {
    position : {
        x: 0,
        y: 0
    }
}

