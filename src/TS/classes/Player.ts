import { c, camera, canvas, gravity, keys, scaleBG } from "../const";
import { Sprite } from "./Sprite";

export class Players extends Sprite {
    velocity: { x: number, y: number };
    collisions: any[];
    camerabox: { position: { x: number; y: number; }; width: number; height: number; };
    animations: any;
    lastDirection: string;

    constructor(px: number, py: number, col: any[], ImageSrc: string, frameRate: number, animations: any) {
        super(px, py, ImageSrc, frameRate);
        this.position = {
            x: px,
            y: py
        }
        this.velocity = {
            x: 0,
            y: 1
        }
        this.camerabox = {
            position: {
                x: this.position.x,
                y: this.position.y,
            },
            width: 200,
            height: 80,
        }

        this.collisions = col
        this.animations = animations
        this.lastDirection = 'right'

        for (let index in this.animations) {
            const image = new Image()
            image.src = this.animations[index].imageSrc;
            this.animations[index].image = image


        }

    }

    switchSprite(key: string) {
        if (this.image === this.animations[key] || !this.loaded) return;
        this.image = this.animations[key].image
        this.frameBuffer = this.animations[key].frameBuffer;
        this.frameRate = this.animations[key].frameRate;

    }



    updateCameraBox() {
        this.camerabox = {
            width: 300,
            height: 120,
            position: {
                x: this.position.x - (this.camerabox.width / 4),
                y: this.position.y - (this.camerabox.height / 2),
            },
        }
    }

    shouldPanSceneToLeft() {
        const cameraboxRightSide = this.camerabox.position.x + this.camerabox.width
        const scaledDownCanvasWidth = canvas.width / scaleBG

        if (cameraboxRightSide >= 1599) return

        if (cameraboxRightSide >= scaledDownCanvasWidth + Math.abs(camera.position.x)) {
            camera.position.x -= this.velocity.x
        }
    }
    shouldPanSceneToRight() {
        if (this.camerabox.position.x <= 0) return
        if (this.camerabox.position.x <= Math.abs(camera.position.x)) {
            camera.position.x -= this.velocity.x
        }
    }

    update() {
        this.updateFrame()
        this.draw()
        this.position.x += this.velocity.x
        this.checkHorizontalCollision()
        this.applyGravity()
        this.checkVerticalCollision()
        this.updateCameraBox()
        c.fillStyle = 'rgba(0, 0, 255, 0)'
        c.fillRect(
            this.camerabox.position.x,
            this.camerabox.position.y,
            this.camerabox.width,
            this.camerabox.height
        )
    }
    applyGravity() {
        this.velocity.y += gravity
        this.position.y += this.velocity.y
    }
    checkVerticalCollision() {
        for (let i = 0; i < this.collisions.length; i++) {
            const collisionNow = this.collisions[i]
            if (this.position.y + this.height >= collisionNow.y &&
                this.position.y <= collisionNow.y + collisionNow.height &&
                this.position.x <= collisionNow.x + collisionNow.width &&
                this.position.x + this.width >= collisionNow.x) {
                // quand on arrive sur une collision par le haut la velocity est positive car on dessend
                if (this.velocity.y > 0) {
                    this.velocity.y = 0
                    // 0.001 est une securité 
                    this.position.y = collisionNow.y - this.height - 0.001
                    break
                }
                // si c'est par le bas elle est negative
                if (this.velocity.y < 0) {
                    this.velocity.y = 0
                    this.position.y = collisionNow.y + collisionNow.height + 0.001
                    keys.z.pressed = false;
                    break
                }
            }
        }
    }

    checkHorizontalCollision() {
        for (let i = 0; i < this.collisions.length; i++) {
            const collisionNow = this.collisions[i]
            if (this.position.y + this.height >= collisionNow.y &&
                this.position.y <= collisionNow.y + collisionNow.height &&
                this.position.x <= collisionNow.x + collisionNow.width &&
                this.position.x + this.width >= collisionNow.x) {
                if (this.velocity.x > 0) {
                    this.velocity.x = 0
                    this.position.x = collisionNow.x - this.width - 0.001
                    break
                }
                if (this.velocity.x < 0) {
                    this.velocity.x = 0;
                    this.position.x = collisionNow.x + collisionNow.width + 0.001;
                    break
                }
            }

        }
    }


}
