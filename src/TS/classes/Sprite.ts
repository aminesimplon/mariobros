import { c } from "../const";

export class Sprite {
    // typage
    image: HTMLImageElement;
    width: number;
    height: number;
    position: { x: number; y: number; };
    cropBox: {
        position: { x: number; y: number; }; width: number; height: number;
    };
    frameRate: number;
    currentFrame: number;
    frameBuffer: number;
    elapsedFrame: number;
    loaded: boolean;

    constructor(x: number, y: number, imageSrc: string, frameRate: number = 1) {
        this.position = {
            x: x,
            y: y
        }
        this.loaded = false;
        this.image = new Image()
        this.image.src = imageSrc
        this.image.onload = () => {
            this.width = this.image.width / this.frameRate;
            this.height = this.image.height;
            this.loaded = true;
        }
        this.frameRate = frameRate;
        this.currentFrame = 0;
        this.frameBuffer = 10;
        this.elapsedFrame = 0;

    }
    draw() {
        if (!this.image) {
            return
        }
        const cropBox = {
            position: {
                x: this.currentFrame * (this.image.width / this.frameRate),
                y: 0
            },
            width: this.image.width / this.frameRate,
            height: this.image.height
        }

        c.drawImage(this.image, cropBox.position.x, cropBox.position.y, cropBox.width, cropBox.height, this.position.x, this.position.y, this.width, this.height)
    }
    update() {
        this.draw()

    }

    updateFrame() {
        this.elapsedFrame++;
        if (this.elapsedFrame % this.frameBuffer === 0) {
            if (this.currentFrame < this.frameRate - 1) {
                this.currentFrame++
            } else {
                this.currentFrame = 0
            }
        }

    }
}