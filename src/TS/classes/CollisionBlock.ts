import { c } from "../const";

export class CollisionBlock {
    x: number;
    y: number;
    height: number;
    width: number;

    constructor(x:number, y:number) {
        this.x = x
        this.y = y
        this.width = 16
        this.height = 16
    }
    draw() {
        c.fillStyle = 'rgba(225, 0, 0, 0)'
        c.fillRect(this.x, this.y, this.width, this.height)
    }
    update() {
        this.draw()
    }
}