import { CollisionBlock } from "./classes/CollisionBlock.ts"
import { Players } from "./classes/Player.ts"
import { Sprite } from "./classes/Sprite.ts"
import { c, camera, canvas, collision2D, keys, scaleBG, scaledCanvas } from "./const.ts"
import { collision } from "./data/collision.ts"


// Création du joureur et du background


const background = new Sprite(0, 0, "/images/BG.png")
const collisionBlock: any[] = []
const player = new Players(50, 100, collisionBlock, "/SpriteSheets/MarioStand.png", 1, {
    stand: {
        imageSrc: "/SpriteSheets/MarioStand.png",
        frameRate: 1,
        frameBuffer: 10
    },
    run: {
        imageSrc: "/SpriteSheets/MarioWalk.png",
        frameRate: 3,
        frameBuffer: 10
    },
    jump: {
        imageSrc: "/SpriteSheets/MarioJump.png",
        frameRate: 1,
        frameBuffer: 1
    },
    fall: {
        imageSrc: "/SpriteSheets/MarioFall.png",
        frameRate: 1,
        frameBuffer: 10
    },
    boom: {
        imageSrc: "/SpriteSheets/MarioBoom.png",
        frameRate: 1,
        frameBuffer: 10
    },
    standL: {
        imageSrc: "/SpriteSheets/MarioStandLeft.png",
        frameRate: 1,
        frameBuffer: 10
    },
    runL: {
        imageSrc: "/SpriteSheets/MarioWalkLeft.png",
        frameRate: 3,
        frameBuffer: 10
    },
    jumpL: {
        imageSrc: "/SpriteSheets/MarioJumpLeft.png",
        frameRate: 1,
        frameBuffer: 1
    },
    fallL: {
        imageSrc: "/SpriteSheets/MarioFallLeft.png",
        frameRate: 1,
        frameBuffer: 10
    },

})


// la boucle prend le tableau collision et le divise en 20 tableau, qui represente la hauteur de ma map

for (let i = 0; i < collision.length; i += 100) {
    collision2D.push(collision.slice(i, i + 100))
}

// for each permet de bouclé sur la lenght d'une array

collision2D.forEach((row, y) => {
    row.forEach((symbol: number, x: number) => {
        if (symbol === 211) {
            collisionBlock.push(new CollisionBlock(x * 16, y * 16))
        }
    })
}
)



// La fonction animate est une boucle qui ce lance a chaque frame de la fenêtre

function animate() {
    setTimeout(animate, 5)
    c.fillStyle = '#5C94FC'
    c.fillRect(0, 0, canvas.width, canvas.height)

    /* le scale affecte aussi le canva mais ca ne ce voit pas grace au save et restore */

    c.save()
    c.scale(scaleBG, scaleBG)
    c.translate(camera.position.x, -background.image.height + scaledCanvas.height)
    background.update()

    // On créé les collision ici pour que la scale les affecte

    collisionBlock.forEach((collisionBlock) => {
        collisionBlock.update()
    })

    player.update()

    player.velocity.x = 0

    if (keys.d.pressed) {
        player.switchSprite('run')
        player.velocity.x = 1
        player.lastDirection = 'right'
        player.shouldPanSceneToLeft()
    } else if (keys.q.pressed) {
        player.switchSprite('runL')
        player.velocity.x = -1
        player.lastDirection = 'left'
        player.shouldPanSceneToRight()
    } else if (player.velocity.y === 0) {
        if (player.lastDirection === 'right') player.switchSprite('stand')
        else player.switchSprite('standL')
    }

    if (player.velocity.y < -2 && player.velocity.y != 0) {
        if (player.lastDirection === 'right') {
            player.switchSprite('jump')
        } else {
            player.switchSprite('jumpL')
        }
    } else if (player.velocity.y > -2 && player.velocity.y != 0) {
        if (player.lastDirection === 'right') {
            player.switchSprite('fall')
        } else {
            player.switchSprite('fallL')
        }
    }

    if (keys.z.pressed && player.velocity.y == 0) {
        player.velocity.y = -3
    } else if (keys.s.pressed && player.velocity.y != 0) {
        player.switchSprite('boom')
        player.velocity.y = 2
    }


    c.restore()

    // Implementation du joureur et du background dans le canvas



}
animate()

window.addEventListener('keydown', (e) => {
    switch (e.key) {
        case 'z':
            keys.z.pressed = true;
            break;
        case 'd':
            keys.d.pressed = true;
            break;
        case 'q':
            keys.q.pressed = true;
            break
        case 's':
            keys.s.pressed = true;
            break;
    }
})

window.addEventListener('keyup', (e) => {
    switch (e.key) {
        case 'd':
            keys.d.pressed = false
            break;
        case 'q':
            keys.q.pressed = false
            break
        case 's':
            keys.s.pressed = false
            break
        case 'z':
            keys.z.pressed = false
            break
    }
})



